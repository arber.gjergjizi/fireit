import {Component, OnInit} from '@angular/core';
//declare var grapesjs: any;
// @ts-ignore
import grapesjs from 'grapesjs';
import 'grapesjs-preset-webpage';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  editor = null;
  editor2 = null;

  ngOnInit(): void {

    this.editor = grapesjs.init({
      // Indicate where to init the editor. You can also pass an HTMLElement
      container: '#gjs',
      // Get the content for the canvas directly from the element
      // As an alternative we could use: `components: '<h1>Hello World Component!</h1>'`,
      fromElement: true,
      // Size of the editor
      height: '600px',
      width: 'auto',
      plugins: ['gjs-preset-webpage'],
      storageManager: {
        id: 'gjs-',
        autoload: true,
        stepsBeforeSave: 3,
        contentTypeJson: true
      },
    });
    this.editor2 = grapesjs.init({
      // Indicate where to init the editor. You can also pass an HTMLElement
      container: '#gjs2',
      // Get the content for the canvas directly from the element
      // As an alternative we could use: `components: '<h1>Hello World Component!</h1>'`,
      fromElement: true,
      // Size of the editor
      height: '600px',
      width: 'auto',
      plugins: ['gjs-preset-webpage'],
      storageManager: {
        id: 'gjs2-',
        autoload: true,
        stepsBeforeSave: 3,
        contentTypeJson: true
      },
    });
  }

}
